import React, { useState } from 'react';
import Modal from 'react-modal';
import Order from './OrdersDisplay';
import { Button } from 'react-bootstrap';
import { MdDelete, MdOutlineCloseFullscreen, MdUpdate } from 'react-icons/md';
import { Link } from 'react-router-dom';
interface Order {
  orderId: number;
  orderDate: string;
  totalAmount: number;
  deliveryAddress: string;
  custId: number;
  pizzaList: {
    pizzaName:string,
    size: string;
    quantity: number;
    price: number;
    pizzaId: number;
  }[];
  status: string;
}
interface OrderDetailsModalProps {
  isOpen: boolean;
  onClose: () => void;
  orderDetails: Order | null;
}

const OrderDetailsModal: React.FC<OrderDetailsModalProps> = ({ isOpen, onClose, orderDetails }) => {
 

 
  return (
    <Modal
      isOpen={isOpen}
      onRequestClose={onClose}
      style={{
        overlay: {
          backgroundColor: 'transparent',
          top: '50%', left: '20%', transform: 'translate(-50%, -50%)'
        },
        content: {
          width: '700px',
          margin: 'auto',

        },
      }}

    >
      {orderDetails && (
        <div>
          <h3>Order ID: {orderDetails.orderId}</h3>
          <p>Status: {orderDetails.status}</p>
          <p>Order Date: {orderDetails.orderDate}</p>

          <p>Delivery Address: {orderDetails.deliveryAddress}</p>
          <p>Customer ID: {orderDetails.custId}</p>
          <h4>Pizza List:</h4>
          <ul>
            {orderDetails.pizzaList.map((pizzaItem: any) => (
              <li key={pizzaItem.pizzaId}>
              Pizzaname: {pizzaItem.pizzaName} , Size: {pizzaItem.size}, Quantity: {pizzaItem.quantity}, Price: {pizzaItem.price}
              </li>
            ))}
          </ul>
          <p>Total Amount: {orderDetails.totalAmount}</p>
          <hr />
        </div>
      )}
      <div style={{ display: ' flex',justifyContent : 'space-between'}}> <Button variant='secondary' onClick={onClose}>close< MdOutlineCloseFullscreen/></Button>
      <Link to="/"><Button variant='info'>update<MdUpdate/></Button></Link>
      <Button variant='danger'>delete<MdDelete /></Button></div>
     
    </Modal>
  );
};

export default OrderDetailsModal;
