import React, { useState, useEffect } from 'react';
import axios from 'axios';
import OrderDetailsModal from './OrderDetailsModal';
import { Button } from 'react-bootstrap';
import { MdRemoveRedEye, MdOutlineAutoDelete, MdUpdate } from 'react-icons/md';
import { Link } from 'react-router-dom';

interface Order {
  orderId: number;
  orderDate: string;
  totalAmount: number;
  deliveryAddress: string;
  custId: number;
  pizzaList: {
    pizzaName:string,
    size: string;
    quantity: number;
    price: number;
    pizzaId: number;
  }[];
  status: string;
}

const DisplayOrders: React.FC = () => {
  const [orders, setOrders] = useState<Order[]>([]);
  const [selectedOrder, setSelectedOrder] = useState<Order | null>(null);

  const tableStyle: React.CSSProperties = {
    width: '100%',
    borderCollapse: 'collapse',
    margin: '10px',
    boxShadow: '0 2px 5px rgba(0, 0, 0, 0.1)',
  };

  const thStyle: React.CSSProperties = {

    // padding: '8px',
    textAlign: 'left',
    borderBottom: '1px solid #ddd',



    border: '1px solid #dee2e6', backgroundColor: 'grey', color: '#FFF', borderBottomWidth: '2px', borderBottomColor: '#212529'
  };

  const tdStyle: React.CSSProperties = {
    padding: '8px',
    textAlign: 'left',
    borderBottom: '1px solid #ddd',

  };

  const buttonStyle: React.CSSProperties = {
    padding: '5px 10px',
    backgroundColor: '#4CAF50',
    color: 'white',
    border: 'none',
    borderRadius: '5px',
    cursor: 'pointer',
  };









  //fetch All orders
  useEffect(() => {
    // Fetch orders from the backend API and set them in the state
    const fetchOrders = async () => {
      try {
        const response = await axios.get<{ data: Order[] }>('http://localhost:8080/orders');
        console.log(response.data.data)
        setOrders(response.data.data); 
      } catch (error) {
        console.error('Error fetching orders:', error);
      }
    };

    fetchOrders();
  }, []);




  // Getting the selected Order
  const handleGetOrderClick = async (orderId: number) => {
    try {
      const response = await axios.get(`http://localhost:8080/orders/${orderId}`);
      const orderDetails = response.data.data; 
      console.log('Order details:', orderDetails);
      setSelectedOrder(orderDetails);
    } catch (error) {
      console.error(`Error fetching order details for orderId ${orderId}:`, error);
    }
  };

  const handleCloseModal = () => {
    setSelectedOrder(null);
  };

 

  return (
    <div>

<table style={tableStyle}>
      <thead>
        <tr >
          <th style={thStyle}>OrderId</th>
          <th style={thStyle}>Status</th>
          <th style={thStyle}>CustomerID</th>
          <th style={thStyle}>Total Amount</th>
          <th style={thStyle}>Delivery Address</th>
          <th style={thStyle}>Get Details</th>
          <th style={thStyle}>Update</th>
          <th style={thStyle}>Delete</th>
        </tr>
      </thead>
      <tbody>
        {orders.map((order) => (
          <tr key={order.orderId}>
            <td style={tdStyle}>{order.orderId}</td>
            <td style={tdStyle}>{order.status}</td>
            <td style={tdStyle}>{order.custId}</td>
            <td style={tdStyle}>{order.totalAmount}</td>
            <td style={tdStyle}>{order.deliveryAddress}</td>
            
            <td>
              <Button variant='success' onClick={() => handleGetOrderClick(order.orderId)}>
               <MdRemoveRedEye />
              </Button>
            </td>
            <td>
            <Link to="/"><Button variant='info'><MdUpdate/></Button></Link>
            </td>
            <td>
              <Button variant='danger' >
                <MdOutlineAutoDelete />
              </Button>
            </td>
          </tr>
        ))}
      </tbody>
    </table>

      <OrderDetailsModal isOpen={selectedOrder !== null} onClose={handleCloseModal} orderDetails={selectedOrder} />
    
    </div>
  );
};

export default DisplayOrders;
