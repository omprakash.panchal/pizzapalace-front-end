import React from 'react';
import { Form, Button, Container, Row, Col } from 'react-bootstrap';

interface AddressFormProps {
  onSubmit: (addressData: { deliveryAddress: string; customerAddress: string }) => void;
}

const AddressForm: React.FC<AddressFormProps> = ({ onSubmit }) => {
  const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    const formData = new FormData(event.currentTarget);
    const addressData = {
      deliveryAddress: formData.get('deliveryAddress') as string,
      customerAddress: formData.get('customerAddress') as string,
    };
    onSubmit(addressData);
  };

  return (
    <Container className='pt-5' >
      <Row>
        <Col xs sm = {6}   lg = {6}>
    <Form onSubmit={handleSubmit}>
      <Form.Group controlId="deliveryAddress">
        <Form.Label>Delivery Address</Form.Label>
        <Form.Control type="text" placeholder="Enter delivery address" name="deliveryAddress" />
      </Form.Group>
      <Form.Group controlId="customerAddress">
        <Form.Label>Customer Address</Form.Label>
        <Form.Control type="text" placeholder="Enter customer address" name="customerAddress" />
      </Form.Group>

      <Form.Group controlId="customerAddress">
        <Form.Label>Customer Address</Form.Label>
        <Form.Control type="text" placeholder="Enter customer address" name="customerAddress" />
      </Form.Group>

      <Form.Group controlId="customerAddress">
        <Form.Label>Customer Address</Form.Label>
        <Form.Control type="text" placeholder="Enter customer address" name="customerAddress" />
      </Form.Group>

      <Form.Group controlId="customerAddress">
        <Form.Label>Customer Address</Form.Label>
        <Form.Control type="text" placeholder="Enter customer address" name="customerAddress" />
      </Form.Group>

      <Form.Group controlId="customerAddress">
        <Form.Label>Customer Address</Form.Label>
        <Form.Control type="text" placeholder="Enter customer address" name="customerAddress" />
      </Form.Group>

      <Form.Group controlId="customerAddress">
        <Form.Label>Customer Address</Form.Label>
        <Form.Control type="text" placeholder="Enter customer address" name="customerAddress" />
      </Form.Group>

      <Form.Group controlId="customerAddress">
        <Form.Label>Customer Address</Form.Label>
        <Form.Control type="text" placeholder="Enter customer address" name="customerAddress" />
      </Form.Group>

      <Button variant="primary" type="submit">
        Submit
      </Button>
    </Form>
    </Col>
    </Row>
    </Container>
  );
};

export default AddressForm;
