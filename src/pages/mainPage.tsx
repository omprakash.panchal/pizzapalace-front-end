import { useEffect, useState } from 'react';
import { Container, Row, Col, Card, Button } from 'react-bootstrap';
import axios from 'axios';
import Product from '../components/product';
import Cart from '../components/cart';
import { Pizza } from '../models/pizza'; // Pizza interface defined in a separate file /model/type
import { Order } from '../models/order';
import { toast } from 'react-toastify';
import { pizzas } from '../models/Pizzas';


interface Orders {
  orderId: number;
  orderDate: string;
  totalAmount: number;
  deliveryAddress: string;
  custId: number;
  pizzaList: {
    size: string;
    quantity: number;
    price: number;
    pizzaId: number;
  }[];
  status: string;
}



const MainPage = () => {


  const [pizzas, setPizzas] = useState<Pizza[]>([]);   // Manage the state of pizza

  const [cartItems, setCartItems] = useState<Pizza[]>([]);  // Manage the state of CartItem
  const [showOrderForm, setShowOrderForm] = useState(false);
  const [showUpdateOrderForm, setShowUpdateOrderForm] = useState(false);
  

  useEffect(() => {
    fetchPizzas();
  }, []);    //For Fetching All pizza

  // Fetching All pizza

  const fetchPizzas = async () => {
    try {
      const response = await axios.get('http://localhost:8080/pizza'); // Replace with your backend API endpoint
      console.log(response.data);
      setPizzas(response.data.data);
    } catch (error) {
      console.error('Error fetching pizzas:', error);
    }
  };



  // Removing Item From cart

  const removeFromCart = (pizzaId: number, size: string) => {
    setCartItems((prevCartItems: Pizza[]) => {
      const updatedCartItems = prevCartItems.filter(
        (item) => item.pizzaId !== pizzaId || item.size !== size
      );

      return updatedCartItems;
    });

  };

  // MAnage the Quantity Based On size

  const handleQuantityChange = (pizzaId: number, newQuantity: number, size: string) => {
    setCartItems((prevCartItems) => {
      const updatedCartItems = prevCartItems.map((item) => {
        if (item.pizzaId === pizzaId && item.size === size) {
          return { ...item, quantity: newQuantity };
        }
        return item;
      });
      return updatedCartItems;
    });
  };


  // Add Item In Cart

  const addToCart = (pizza: Pizza, size: string, quantity: number, price: number) => {

    setCartItems((prevCartItems: Pizza[]) => {

      const existingItem = prevCartItems.find((item) => item.pizzaId === pizza.pizzaId && item.size === size);

      if (existingItem) {
        // Item already exists, update the quantity for the existing size
        const updatedCartItems = prevCartItems.map((item) => {
          if (item.pizzaId === pizza.pizzaId && item.size === size) {
            return { ...item, quantity: item.quantity + quantity };
          }
          return item;
        });
        return updatedCartItems;
      } else {
        // Item does not exist, add a new item with the selected size
        const cartItem = { ...pizza, size, quantity, price };
        return [...prevCartItems, cartItem];
      }
    });
  };



// This is Converting pizza To pizzas
  const convertToPizzaType = (pizzaList: {
    size: string;
    quantity: number;
    price: number;
    pizzaId: number;
  }[]): Pizza[] => {
    return pizzaList.map((pizzaItem) => ({
      pizzaId: pizzaItem.pizzaId,
      size: pizzaItem.size,
      quantity: pizzaItem.quantity,
      price: pizzaItem.price,
      description: '',
      imageUrl: '',
      name: '',
      pizzaType: '',
      priceLSize: 0,
      priceMSize: 0,
      pizzaName: ' ',
      priceRegSize: 0,





    }));
  };



  // Function to add items from the last order to the cart
  const addItemsFromLastOrderToCart = (lastOrder: Orders ) => {
    
    if (lastOrder) {

  
      const updatedCartItems = convertToPizzaType([...lastOrder.pizzaList]);
      setCartItems(updatedCartItems);

      toast.success('Add new Items In order!', {
        position: toast.POSITION.TOP_RIGHT,
        autoClose: 2000,
      });
      
    }
  };



  // clearing cart
  const clearCartItems = () => {
    setCartItems([]);
  };

// Calculatinng Subtotal

  const calculateSubtotal = (): number => {
    let subtotal = 0;
    cartItems.forEach((item) => {
      const price = item.price * item.quantity;
      subtotal += price;
    });
    return subtotal;
  };

  // Handle  close button for closing form
  const handleCloseOrderForm = () => {
    setShowOrderForm(false);
  };

  return (
    <Container fluid style={{ margin: "auto", paddingTop: "60px", backgroundColor: "#EEE" }}>
      <Row>
        <Col style={{margin:'20px'}}xs sm={8} lg={8}>
          <h3>Pizza</h3>

        </Col>



      </Row>
      <Row>
        <Col xs="auto" sm={8} lg={8} >
          <Card style={{ backgroundColor: "#EEE" }}>
            <Row >
              {pizzas.map((pizza) => (
                <Col key={pizza.pizzaId} >

                  {/* Sending To Product Component */}
                  <Product key={pizza.pizzaId} pizza={pizza} addToCart={addToCart

                  } />

                </Col>
              ))}
            </Row>
          </Card>
        </Col>
        <Col xs sm={4} lg={4}>
          <Card style={{ backgroundColor: "#EEE" }}>

            {/* Sending To Cart Component */}


            <Cart cartItems={cartItems} handleQuantityChange={handleQuantityChange} removeFromCart={removeFromCart} addItemsFromLastOrderToCart = {addItemsFromLastOrderToCart} clearCartItems={clearCartItems} />

          </Card>
        </Col>
      </Row>

    </Container>
  );
};

export default MainPage;