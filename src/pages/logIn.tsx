
import { Alert, Button, Card, Col, Container, Form, Row, Spinner } from "react-bootstrap"
import { Link, NavLink, useNavigate } from "react-router-dom"

import { ChangeEvent, JSXElementConstructor, ReactElement, ReactNode, ReactPortal, useContext, useState } from "react"
import { ToastContentProps, toast } from "react-toastify"
import axios, { AxiosRequestConfig } from "axios"
// import { loginUser } from "../services/user.service"
// import UserContext from "../context/UserContext"
// const Login = () => {


// const redirect = useNavigate()
// // const userContext = useContext(UserContext);

// let [data, setData] = useState({
//     email: '',
//     password: ''
// })
// let [error, setError] = useState({
//     errorData: null,
//     isError: false
// })

// let [loading, setLoading] = useState(false)

// const handleChange = (event: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>, property: string) => {
//     setData({
//         ...data,
//         [property]: event.target.value
//     })
// }

// const handleReset = () => {

//     setData({
//         email: '',
//         password: ''
//     })

//     setError({
//         errorData: null,
//         isError: false
//     })

//     setLoading(false)
// }

//submit form

// const submitForm = (event: { preventDefault: () => void }) => {
//     event.preventDefault();import { loginUser } from "../services/user.service"
//     // import UserContext from "../context/UserContext"
//     // const Login = () => {

//     console.log(data)

//     //client side validations

//     if (data.email === undefined || data.email.trim() === '') {
//         toast.error("Email required !!")
//         return
//     }

//     if (data.password === undefined || data.password.trim() === '') {
//         toast.error("Password required !!")
//         return
//     }

//     //login api

//     setLoading(true)
//     loginUser(data)
//         .then((data: any) => {
//             console.log(data)
//             toast.success("Logged In")
//             setError({
//                 errorData: null,
//                 isError: false
//             })

//             //redirect to dashboard page:
//             // 1. normal : normal user ke dashboard per le jana hai 



//             //home dashboard page
//             // / users / home

//             // userContext.setIsLogin(true)
//             // userContext.setUserData(data)
//             // userContext.login(data)

//             redirect("/users/home")


//             // 2. admin : admin user dashabord per le jana hai 
//         })
//         .catch((error: { response: { data: { message: string | number | boolean | ReactElement<any, string | JSXElementConstructor<any>> | Iterable<ReactNode> | ReactPortal | ((props: ToastContentProps<unknown>) => ReactNode) | null | undefined } } }) => {
//             console.log(error)
//             toast.error(error.response.data.message)
//             setError({
//                 // errorData: error,
//                 isError: true
//             })
//         })
//         .finally(() => {
//             setLoading(false)
//         })

// }





const LoginForm = () => {
    const [data, setData] = useState({
        email: '',
        password: ''
    })

    const handleReset = () => {

        setData({
            email: '',
            password: ''
        });
    }



    const handleChange = (event: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>, property: string) => {
        setData({
            ...data,
            [property]: event.target.value
        })
    }




    const submitForm = (event: { preventDefault: () => void }) => {
        event.preventDefault();
        // import UserContext from "../context/UserContext"
        // const Login = () => {

        console.log(data)

        //client side validations

        if (data.email === undefined || data.email.trim() === '') {
            toast.error("Email required !!")
            return
        }

        if (data.password === undefined || data.password.trim() === '') {
            toast.error("Password required !!")
            return
        }
        if (data) {
            axios.post('http://localhost:8080/customers/email', data)
                .then(response => {
                    // Handle the response here
                    console.log(response.data);
                    toast.success("Logged In successfully");
                    window.location.href = '/';
                })
                .catch(error => {
                    // Handle errors here
                    console.error(error);
                    toast.error(`Something went wrong! ${error}`);
                });
        }



    }






    return (

        <Container style={{
            marginTop: '120px'

        }}>


            <Row>
                <Col md={{
                    span: 8,
                    offset: 2
                }}>



                    <Card className="my-3 border-0 shadow" style={{
                        // position: "relative",
                        // top: -60

                    }}>

                        <Card.Body>

                            {/* {JSON.stringify(userContext)} */}



                            <h3 className="text-center text-uppercase">Login </h3>

                            {/* <Alert className="mt-3" onClose={() => setError({
                                    isError: false,
                                    errorData
                                        : null
                                })} dismissible variant="danger" show={error.isError}>

                                    <Alert.Heading>Hey there ,</Alert.Heading>
                                    <p> {error.errorData?.response?.data?.message}</p>

                                </Alert> */}



                            <Form noValidate onSubmit={submitForm} >

                                {/* email login field */}

                                <Form.Group className="mb-3">
                                    <Form.Label>Enter Email </Form.Label>
                                    <Form.Control
                                        type="email"
                                        placeholder="Enter here"
                                        onChange={(event) => handleChange(event, 'email')}
                                        value={data.email}
                                    />
                                </Form.Group>


                                {/* password login field */}

                                <Form.Group className="mb-3">
                                    <Form.Label>Enter Password </Form.Label>
                                    <Form.Control
                                        type="password"
                                        placeholder="Enter here"
                                        onChange={(event) => handleChange(event, 'password')}
                                        value={data.password}

                                    />
                                </Form.Group>


                                <Container className="text-center">
                                    {/* <p>Forget Password ! <a href="/forget">Click here</a></p> */}
                                    <p>If not registered !  <NavLink to="/register" >Click here</NavLink></p>
                                </Container>

                                <Container className="text-center">

                                    <Button type="submit" className="" variant="success" >
                                        {/* <Spinner
                                                animation="border"
                                                size="sm"
                                                // hidden={!loading}
                                                className={'me-2'}

                                            /> */}
                                        {/* <span hidden={!loading}>Please wait...</span> */}

                                        <span>Login</span>

                                    </Button>

                                    <Button className="ms-2" variant="danger" onClick={handleReset}>Reset</Button>
                                </Container>

                            </Form>


                        </Card.Body>

                    </Card>



                </Col>

            </Row>

        </Container>

    )
}

// }

export default LoginForm