import axios from 'axios';
import React, { useEffect, useState } from 'react';
import { Button, Col, Container, Row } from 'react-bootstrap';
import DisplayOrders from './DisplayOrder';
import LastOrder from '../components/LastOrder';



const OrdersDisplay = () => {


    return (
        <>




            <Container fluid style={{ margin: "auto", justifyContent: 'space-between', paddingTop: "60px", backgroundColor: "#EEE" }}>
                <Row>
                 
                    <Col xs="auto" sm={6} lg={6} style={{marginTop:'50px', height: '600px', marginLeft: "100px", backgroundColor: 'white', overflowY: 'auto' }}>
                        <div style={{ zIndex: '2', backgroundColor: "LightGray",padding:'20px', position: "sticky", top: '0' }}>

                            <h3>All Orders</h3>



                        </div>
                        <div> <DisplayOrders /></div>
                    </Col>

                    <Col xs sm={5} lg={5} style={{ height: '600px', backgroundColor: '#EEE', overflowY: 'auto'  ,marginTop: '20px'}}>
                        <h3 style={{  backgroundColor: "LightGray",marginTop: "30px",padding:'20px'}}>Get Last Order </h3> 
                           <div style={{  backgroundColor: "LightGray",padding:'20px'}}>< LastOrder  /> </div>
                                               
                    </Col>


                </Row>

            </Container>

        </>
    );
};

export default OrdersDisplay;
