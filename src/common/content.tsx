import {
    BrowserRouter,
    Routes,
    Route,
} from "react-router-dom";
import MainPage from "../pages/mainPage";

import MainCart from "../pages/mainCart";
import AddressForm from "../pages/AddressForm";
import { Cart } from "react-bootstrap-icons";

import Order from "../pages/OrdersDisplay";
import { pizzas } from "../models/Pizzas";
import CreateOrders from "../pages/OrdersDisplay";
import LastOrder from "../components/LastOrder";
import OrdersDisplay from "../pages/OrdersDisplay";
import Login from "../pages/logIn";
import Register from "../pages/register";







const Content = () => {




    return (
        <>
            <BrowserRouter>
                <Routes>
                
                    <Route path="/" Component={MainPage}>
                    </Route>
                    <Route path="/order" element={<OrdersDisplay />}>
                    </Route>
                    <Route path="/login" element={<Login />} />
                    <Route path="/register" element={<Register />} />



                </Routes>
            </BrowserRouter>
        </>
    )
}

export default Content;

function calculateSubtotal(): number {
    throw new Error("Function not implemented.");
}
