
import Navbar from 'react-bootstrap/Navbar';
import Container from 'react-bootstrap/Container';
import { Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';

const NavbarComponent = () => {
    return  <Navbar fixed="top" className="bg-body-tertiary mb-5">
                <Container>
                    <Navbar.Brand ><b>Pizza Hub</b></Navbar.Brand>
                    <Navbar.Brand href="/">home</Navbar.Brand>
                    <Navbar.Collapse className="justify-content-end">
                    <Navbar.Text>
                    <Navbar.Brand href="/order">All Orders</Navbar.Brand>
                    <Navbar.Brand href="/login">LogIn</Navbar.Brand>
                    <Navbar.Brand href="/register">Sign Up</Navbar.Brand>
                        </Navbar.Text>
                        {/* <Navbar.Text>
                        Signed in as: <a href="#login"> User 1</a>
                        </Navbar.Text> */}
                    </Navbar.Collapse>
                </Container>
            </Navbar>
}

export default NavbarComponent;