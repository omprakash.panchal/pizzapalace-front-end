export interface OrderItem {
    lineId: number;
    orderId: number;
    size: string;
    quantity: number;
    totalLineAmount: number;
    pizzaId: number;
  }