export interface Order {
 
  orderId: number;
  status: string;
  orderDate: Date;
  totalAmount: number;
  deliveryAddress: string;
  custId: number;
}