export interface Pizza {
  
    // itemId(itemId: any, arg1: number): void;
    pizzaId: number;
    pizzaName: string;
    description: string;
    pizzaType: string;
    imageUrl: string;
    priceRegSize: number;
    size: string;
    quantity: number;
    priceMSize: number;
    priceLSize: number;
    price: number;
  }