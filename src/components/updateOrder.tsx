

import React, { useEffect, useState } from 'react';
import { Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import axios from 'axios';
import { Pizza } from '../models/pizza';
import { pizzas } from '../models/Pizzas';
import { toast } from 'react-toastify';

interface Order {
  orderId: number;
  orderDate: string;
  totalAmount: number;
  deliveryAddress: string;
  custId: number;
  pizzaList: {
    size: string;
    quantity: number;
    price: number;
    pizzaId: number;
    pizzaName: string
  }[];
  status: string;
}

interface UpdateOrderProps {
  cartItems: Pizza[];
  subTotal: number;
}

const UpdateOrder: React.FC<UpdateOrderProps> = ({ cartItems, subTotal }) => {
  const lastOrderString = localStorage.getItem('lastOrder');
  const lastOrder: Order = lastOrderString ? JSON.parse(lastOrderString) : null;

  const [orderData, setOrderData] = useState<Order>({
    orderId: lastOrder.orderId,
    status: lastOrder.status,
    orderDate: lastOrder.orderDate,
    totalAmount: subTotal,
    deliveryAddress: lastOrder.deliveryAddress,
    custId: lastOrder.custId,
    pizzaList: [],

  });


//This is For Adding the cartItem to cart From lastOrder

  useEffect(() => {

    // Function to add items from the cart to the orderData
    const addItemsToOrderData = () => {
      // Update the 'pizzaList' property of 'orderData' with the current 'cartItems'
      setOrderData((prevOrderData) => ({
        ...prevOrderData,
        pizzaList: cartItems,
        totalAmount: subTotal,// Assign the 'cartItems' to 'pizzaList'
      }));
    };
    // Call the function to update 'orderData' whenever 'cartItems' changes
    addItemsToOrderData();
  }, [cartItems]);



  // Handling the Updatated Input Data
  const handleInputChange = (
    event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement | HTMLSelectElement>
  ) => {
    const { name, value } = event.currentTarget;
    setOrderData((prevOrderData) => ({
      ...prevOrderData!,
      [name]: value,
    }));
  };

  // updating Order Data
  const updateOrder = async () => {
    try {
      if (orderData) {
        // Assuming you have an API endpoint to update the order
        await axios.post(`http://localhost:8080/orders`, orderData);
        console.log(orderData)
        // Clear local storage and reset orderData after successful update
        localStorage.removeItem('lastOrder');
        // Optionally, you can show a success message or redirect to a success page
        toast.success('Order updated successfully!');
      }
    } catch (error) {
      toast.error('Something went wrong while updating!');
      console.error('Error updating order:', error);
    }
  };

  return (
    <div style={{ alignItems: 'center', justifyContent: 'center', backgroundColor: "#EEE", padding: '5px , auto', overflowY: 'auto' }} className="container mt-3 ">
      <h2>Update Order</h2>
      {orderData ? (
        <form>
          <div className="form-group">
            <label>Order ID:</label>
            <input className="form-control" type="number" name="orderId" value={orderData.orderId} onChange={handleInputChange} />
          </div>



          <div className="form-group">
            <label>Total Amount:</label>
            <label>{subTotal}</label>
          </div>

          <div>
            <label>Status:</label><br />
            <select style={{ width: '300px', height: '3rem' }} name="status" value={orderData.status} onChange={handleInputChange}>
              <option defaultChecked value="">Select status</option>
              <option value="Pending">Pending</option>
              <option value="Processing">Processing</option>
              <option value="Completed">Completed</option>
            </select>
          </div>

          <div className="form-group">
            <label>Delivery Address:</label>
            <textarea className="form-control" name="deliveryAddress" value={orderData.deliveryAddress} onChange={handleInputChange} />
          </div>

          <div className="form-group">
            <label>Customer ID:</label>
            <input className="form-control" type="number" name="custId" value={orderData.custId} onChange={handleInputChange} />
          </div>

          <Link to="/order">
            <Button className="btn btn-primary m-3" onClick={updateOrder}>Place Order</Button>
          </Link>
          <Button className="btn btn-primary m-3" variant="secondary"  >Cancel</Button>
        </form>
      ) : (
        <p>Loading...</p>
      )}
    </div>



  );
};

export default UpdateOrder;
