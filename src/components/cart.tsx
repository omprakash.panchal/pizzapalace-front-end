
import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import ListGroup from 'react-bootstrap/ListGroup';
import { Pizza } from '../models/pizza';
import { Table } from 'react-bootstrap';
import { useState } from 'react';
import { MdDelete } from 'react-icons/md';
import CreateOrder from './CreateOrder';
import UpdateOrder from './updateOrder';
import { toast } from 'react-toastify';
import { Order } from '../models/order';

interface Orders {
  orderId: number;
  orderDate: string;
  totalAmount: number;
  deliveryAddress: string;
  custId: number;
  pizzaList: {
    size: string;
    quantity: number;
    price: number;
    pizzaId: number;
  }[];
  status: string;
}


interface CartProps {
  cartItems: Pizza[];
  handleQuantityChange: (pizzaId: number, newQuantity: number, size: string) => void;
  removeFromCart: (itemId: number, size: string) => void;
  clearCartItems: () => void;
  addItemsFromLastOrderToCart: (lastOrder : Orders) => void;


}

const Cart: React.FC<CartProps> = ({ cartItems, removeFromCart, handleQuantityChange, clearCartItems ,addItemsFromLastOrderToCart}) => {
  
  const [showOrderForm, setShowOrderForm] = useState(false);
  const [showUpdateOrderForm, setShowUpdateOrderForm] = useState(false);
  const lastOrderString = localStorage.getItem('lastOrder');
  const lastOrder = lastOrderString ? JSON.parse(lastOrderString) : null;






  const calculateSubtotal = (): number => {
    let subtotal = 0;
    cartItems.forEach((item) => {
      const price = item.price * item.quantity;
      subtotal += price;
    });
    return subtotal;
  };



  // This is Converting pizza To pizzas
  const convertToPizzaType = (pizzaList: {
    size: string;
    quantity: number;
    price: number;
    pizzaId: number;
  }[]): Pizza[] => {
    return pizzaList.map((pizzaItem) => ({
      pizzaId: pizzaItem.pizzaId,
      size: pizzaItem.size,
      quantity: pizzaItem.quantity,
      price: pizzaItem.price,
      description: '',
      imageUrl: '',
      name: '',
      pizzaType: '',
      priceLSize: 0,
      priceMSize: 0,
      pizzaName: ' ',
      priceRegSize: 0,





    }));
  };





  //Handling Quantity Increment

  const handleQuantityIncrement = (pizzaId: number, quantity: number, size: string) => {
    handleQuantityChange(pizzaId, quantity + 1, size);
  };


  //Handling Quantity Decrement
  const handleQuantityDecrement = (pizzaId: number, quantity: number, size: string) => {
    if (quantity > 1) {
      handleQuantityChange(pizzaId, quantity - 1, size);

    }
  };



  // Function to handle the "Create Order" button click
  const handleCreateOrderBtn = () => {
    setShowOrderForm(true);

  };


  // Function to handle the "Create Order" button click
  const handleUpdateOrderBtn = () => {
    addItemsFromLastOrderToCart(lastOrder)
    setShowUpdateOrderForm(true);

  };


  // Function to handle closing the order form
  const handleCloseOrderForm = () => {

    setShowOrderForm(false);

  };




  return (
    <Card style={{ margin: '10px', height: '600px', overflowY: 'auto', position: "fixed" }}>
      <Card.Body style={{ paddingBottom: "70px", position: "relative" }}>
        <div style={{ zIndex: '2', backgroundColor: "#EEE", position: "sticky", top: '0' }}>
          <Card.Title  >Your Order</Card.Title>


          <svg xmlns="http://www.w3.org/2000/svg" width="100" height="100" fill="currentColor" className="bi bi-cart" viewBox="0 0 16 16">
            <path d="M0 1.5A.5.5 0 0 1 .5 1H2a.5.5 0 0 1 .485.379L2.89 3H14.5a.5.5 0 0 1 .491.592l-1.5 8A.5.5 0 0 1 13 12H4a.5.5 0 0 1-.491-.408L2.01 3.607 1.61 2H.5a.5.5 0 0 1-.5-.5zM3.102 4l1.313 7h8.17l1.313-7H3.102zM5 12a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm7 0a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm-7 1a1 1 0 1 1 0 2 1 1 0 0 1 0-2zm7 0a1 1 0 1 1 0 2 1 1 0 0 1 0-2z" />
          </svg>
        </div>
        <div>

          <Card.Text>
            {cartItems.length === 0 ? (
              <h5>Cart is empty. Add Products</h5>
            ) : (

              <Table>

                <thead  >
                  <tr>
                    <th>Image</th>
                    <th>Pizza Name</th>
                    <th>Price</th>
                    <th >Quantity</th>
                    <th>Total</th>
                    <th>Delete</th>
                  </tr>
                </thead>

                <tbody>
                  {cartItems.map((item) => (
                    <tr key={item.pizzaId}>
                      <td>
                        <img src={item.imageUrl} alt="Pizza" width="100" height="100" />
                      </td>
                      <td>{item.pizzaName}<br />
                        {item.size}</td>

                      <td>{item.price}</td>

                      <td>

                        <Button variant="danger" onClick={() => handleQuantityDecrement(item.pizzaId, item.quantity, item.size)}>
                          <span> - </span>
                        </Button>
                        <span> {item.quantity} </span>
                        <Button variant="primary" onClick={() => handleQuantityIncrement(item.pizzaId, item.quantity, item.size)}>
                          <span> + </span>
                        </Button>



                      </td>
                      {/* <td>{item.quantity}</td> */}
                      <td rowSpan={2}>{item.quantity * item.price}</td>
                      <td>   <Button variant="danger" className="btn-danger" onClick={() => removeFromCart(item.pizzaId, item.size)}><MdDelete /></Button></td>

                    </tr>
                  ))}
                </tbody>
              </Table>



            )}

          </Card.Text>
        </div>
        <div style={{ marginTop: "70px", position: "sticky" }} >


          <ListGroup className="list-group list-group-flush">
            <ListGroup.Item as="li" >Subtotal : {(calculateSubtotal())}</ListGroup.Item>
          </ListGroup>

    


          <Button style={{ marginTop: "20px" }} variant="primary" onClick={handleCreateOrderBtn} >checkout Order</Button>
          {showOrderForm && <CreateOrder cartItems={cartItems} subTotal={calculateSubtotal()} />}

          <Button style={{ marginTop: "20px", marginLeft: '20px' }} variant="primary" onClick={handleUpdateOrderBtn} >Update Order</Button>
          {showUpdateOrderForm && <UpdateOrder cartItems={cartItems} subTotal={calculateSubtotal()} />}
        </div>
      </Card.Body>
    </Card>
  );
}

export default Cart;



