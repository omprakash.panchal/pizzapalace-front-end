

import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import axios from 'axios';
import { Button, Container } from 'react-bootstrap';


interface PizzaItem {
  size: string;
  quantity: number;
  price: number;
  pizzaId: number;
}



interface Order {
  orderId: number;
  orderDate: string;
  totalAmount: number;
  deliveryAddress: string;
  custId: number;
  pizzaList: {  
    size: string;
    quantity: number;
    price: number;
    pizzaId: number;
    pizzaName: string,
  }[];
  status: string;
}

const LastOrder = () => {
  const [lastOrder, setLastOrder] = useState<Order | null>(null);
  const [visible, setVisible] = useState(true);



  // Styling For Table -----------------
  const table: React.CSSProperties = {
    width: '100%',
    borderCollapse: 'collapse',
    margin: '10px',
    boxShadow: '0 2px 5px rgba(0, 0, 0, 0.1)',
    display: 'none'
  };


  const thStyle: React.CSSProperties = {

    // padding: '8px',
    textAlign: 'left',
    borderBottom: '2px solid #ddd',
    border: '1px solid #dee2e6', backgroundColor: 'grey',
     color: '#FFF', borderBottomWidth: '2px', borderBottomColor: '#212529'
  };

  const tdStyle: React.CSSProperties = {
    padding: '8px',
    textAlign: 'left',
    borderBottom: '1px solid #ddd',

  };

  const buttonStyle: React.CSSProperties = {
    padding: '5px 10px',
    backgroundColor: '#4CAF50',
    color: 'white',
    border: 'none',
    borderRadius: '5px',
    cursor: 'pointer',
  };
  //------------------------------------------------

  useEffect(() => {
    const fetchOrders = async () => {
      try {
        const response = await axios.get('http://localhost:8080/orders');
        const orders = response.data.data; // Assuming your API returns the data in a "data" property
        const lastCreatedOrder = orders[0]; // Assuming the orders are sorted by creation date, so the last order will be at the end of the array
       console.log(lastCreatedOrder)
        setLastOrder(lastCreatedOrder);

        localStorage.setItem('lastOrder', JSON.stringify(lastCreatedOrder));

        if (visible) {
          setTimeout(() => {
            // const table: React.CSSProperties = {
            //   display: 'none'
            // };
            setVisible(false)
          }, 5000); // Display for 5 seconds
        }

      } catch (error) {
        console.error('Error fetching orders:', error);
      }
    };

    fetchOrders();
  }, []);



  return (



    <div className="container mt-3">
      {lastOrder ? (
        <table className={`table ${visible ? 'visible' : 'hidden'}`}>
          <tbody>
            <tr>
              <th style={thStyle}>Order ID:</th>
              <td style={tdStyle}>{lastOrder.orderId}</td>
            </tr>
            <tr>
              <th style={thStyle}>Status:</th>
              <td style={tdStyle}>{lastOrder.status}</td>
            </tr>
            <tr>
              <th style={thStyle}>Customer Id:</th>
              <td style={tdStyle}>{lastOrder.custId}</td>
            </tr>
            <tr>
              <th style={thStyle}>Delivery Address:</th>
              <td style={tdStyle}>{lastOrder.deliveryAddress}</td>
            </tr>
            <tr>
              <th style={thStyle}>Total Amount:</th>
              <td style={tdStyle}>{lastOrder.totalAmount}</td>
            </tr>
            <tr>
              <th style={thStyle}>Pizza List:</th>


              <td>
                <div className='text-center'> <table className="table text-center">
                  <thead>
                    <tr>
                      <th  style={thStyle}>Pizza ID</th>
                      <th  style={thStyle}>Pizza Name</th>
                      <th  style={thStyle}>Size</th>
                      <th  style={thStyle}>Quantity</th>
                      <th  style={thStyle}>Price</th>
                    </tr>
                  </thead>
                  <tbody>
                    {lastOrder.pizzaList.map((pizzaItem) => (
                      <tr key={pizzaItem.pizzaId}>
                        <td style={tdStyle}>{pizzaItem.pizzaId}</td>
                        <td style={tdStyle}>{pizzaItem.pizzaName}</td>
                        <td style={tdStyle}> {pizzaItem.size}</td>
                        <td style={tdStyle}>{pizzaItem.quantity}</td>
                        <td style={tdStyle}>{pizzaItem.price}</td>
                      </tr>
                    ))}
                  </tbody>
                </table></div>
              </td>






            </tr>
            <tr>
              <td className='text-center' colSpan={2} style={tdStyle}>
                <Link to="/">
                  <Button variant="info" >Update Last Order</Button>
                </Link>
              </td>
            </tr>
          </tbody>
        </table>
      ) : (
        <p>Loading...</p>
      )}
    </div>
  );
};

export default LastOrder;

