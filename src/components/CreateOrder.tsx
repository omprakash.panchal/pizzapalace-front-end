import axios from 'axios';
import React, { useEffect, useState } from 'react'
import { Button } from 'react-bootstrap';

import { pizzas } from '../models/Pizzas';
import { Link } from 'react-router-dom';
import { Pizza } from '../models/pizza';
import { toast } from 'react-toastify';



interface Order {

    status: string;
    orderDate: Date;
    totalAmount: number;
    deliveryAddress: string;
    custId: number;
    pizzaList: pizzas[];

}

interface CreateOrderProps {
    cartItems: Pizza[];
    // onClose: () => void;

    subTotal: number;
}

const CreateOrder: React.FC<CreateOrderProps> = ({ cartItems, subTotal }) => {

    const [orderData, setOrderData] = useState<Order>({

        status: '',
        orderDate: new Date(),
        totalAmount: subTotal,
        deliveryAddress: '',
        custId: 0,
        pizzaList: [],
        // Initialize with an empty array
    });
    useEffect(() => {
        // Function to add items from the cart to the orderData
        const addItemsToOrderData = () => {
            // Update the 'pizzaList' property of 'orderData' with the current 'cartItems'
            setOrderData((prevOrderData) => ({
                ...prevOrderData,
                pizzaList: cartItems, // Assign the 'cartItems' to 'pizzaList'
            }));
        };

        // Call the function to update 'orderData' whenever 'cartItems' changes
        addItemsToOrderData();
    }, [cartItems]);

    // Calling Back-End Api For Saving Data In database
    const createOrder = async () => {
        try {

            const orderResponse = await axios.post('http://localhost:8080/orders', orderData);
            toast.success("Order placed SuccessFully")
            console.log(orderResponse.data);
            cartItems = [];


        } catch (error) {
            toast.error('Error creating order and saving order line items:');
            console.error('Error creating order and saving order line items:', error);
        }
    };




    //Handling the inputdata using event 
    const handleInputChange = (event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement | HTMLSelectElement>) => {
        const { name, value } = event.currentTarget;
        setOrderData((prevOrderData) => ({
            ...prevOrderData,
            [name]: value,
        }));
    };
    return (

        <div style={{ alignItems: 'center', justifyContent: 'center', backgroundColor: "#EEE", padding: '5px , auto', overflowY: 'auto' }} className="container mt-3 ">
            <h2>Create Order</h2>
            <form >

                <div className="form-group">
                    <label>Order Date:</label>
                    <label>{orderData.orderDate.toISOString().split('T')[0]}</label>
                </div>
                <div className="form-group">
                    <label>Total Amount:</label>
                    <label>{subTotal}</label>
                </div>
                <div >
                    <label>Status:</label><br />
                    <select style={{ width: '300px', height: '3rem' }} name="status" value={orderData.status} onChange={handleInputChange}>
                        <option defaultChecked value="">Select status</option>
                        <option value="Pending">Pending</option>
                        <option value="Processing">Processing</option>
                        <option value="Completed">Completed</option>
                    </select>
                </div>

                <div className="form-group">
                    <label>Delivery Address:</label>
                    <textarea className="form-control" name="deliveryAddress" value={orderData.deliveryAddress} onChange={handleInputChange} />
                </div>
                <div className="form-group">
                    <label>Customer ID:</label>
                    <input className="form-control" type="number" name="custId" value={orderData.custId} onChange={handleInputChange} />
                </div>
                <Link to="/order">
                    <Button className="btn btn-primary m-3" onClick={createOrder} >Place Order</Button>
                </Link>
                <Button className="btn btn-primary m-3" variant="secondary"  >Cancel</Button>
            </form>
        </div>



    )
}


export default CreateOrder;