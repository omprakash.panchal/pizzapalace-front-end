


import React, { useState } from 'react';
import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import { Pizza } from '../models/pizza';
import { ListGroup } from 'react-bootstrap';
import { toast } from 'react-toastify';

interface ProductProps {
  pizza: Pizza;
  addToCart: (pizza: Pizza, size: string, quantity: number, price: number) => void;


}

const Product: React.FC<ProductProps> = ({ pizza, addToCart }) => {
  const [selectedPizza, setSelectedPizza] = useState<Pizza | null>(null);  //Selecet PIzza To add in cart
  const [selectedSize, setSelectedSize] = useState('');   //Selecet Size To add in cart
  const [price, setPrice] = useState(pizza.priceRegSize); //Selecet price To add in cart
  const [selectedQuantity, setSelectedQuantity] = useState(1);  //Selecet Quantity To add in cart


  //Logic for Handle Add to Cart 

  const handleAddToCart = () => {
    const price = handlePrice(selectedSize);
    if (selectedSize && selectedQuantity > 0 && price > 0) {
      addToCart(pizza, selectedSize, selectedQuantity, price);
      setSelectedSize('');
      setSelectedQuantity(1);
      setPrice(price);
    }
    else {
      toast.success("Please Select Size",
        {
          position: 'top-center',

        });
    }
  };







  //Logic for Handle price based on size

  const handlePrice = (selectedSize: string): number => {
    const priceMapping: Record<string, number> = {
      Regular: pizza.priceRegSize,
      Medium: pizza.priceMSize,
      Large: pizza.priceLSize,
    };
    return priceMapping[selectedSize as keyof typeof priceMapping] || 0;
  };



  const handleQuantityIncrement = () => {
    setSelectedQuantity((prevQuantity) => prevQuantity + 1);
  };

  const handleQuantityDecrement = () => {
    if (selectedQuantity > 1) {
      setSelectedQuantity((prevQuantity) => prevQuantity - 1);
    }
  };


  //Logic for Handle select Pizza 


  const handlePizzaSelection = (pizza: Pizza) => {
    setSelectedPizza(pizza);
  };

  //Logic for Handle select size 
  const handleSizeChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setSelectedSize(event.target.value);

    console.log(event.target.value);
  };





  return (
    <>
      <Card style={{ width: '15rem', margin: '10px' }}>
        <Card.Img variant="top" src={pizza.imageUrl} alt='Pizza Image' />
        <Card.Body>
          <Card.Title>{pizza.pizzaName}</Card.Title>
          <Card.Text>{pizza.description}</Card.Text>
          <div>
            <ListGroup className="list-group list-group-flush">
              <ListGroup.Item as="li">Size:</ListGroup.Item>
              <ListGroup.Item as="li">

                <ListGroup.Item as="li">
                  <div>
                    <label>
                      <input
                        style={{margin: '5px'}}
                        type="radio"
                        name="size"
                        value="Regular"
                        checked={selectedSize === 'Regular'}
                        onChange={handleSizeChange}
                      />
                       Regular ₹{pizza.priceRegSize}
                    </label>
                  </div>
                  <div>
                    <label>
                      <input
                        style={{margin: '5px'}}
                        type="radio"
                        name="size"
                        value="Medium"
                        checked={selectedSize === 'Medium'}
                        onChange={handleSizeChange}
                      />
                       Medium ₹{pizza.priceMSize}
                    </label>
                  </div>
                  <div>
                    <label>
                      <input
                      style={{margin: '5px'}}
                        type="radio"
                        name="size"
                        value="Large"
                        checked={selectedSize === 'Large'}
                        onChange={handleSizeChange}
                      />
                       Large ₹{pizza.priceLSize}
                    </label>
                  </div>
                </ListGroup.Item>

              </ListGroup.Item>

              <ListGroup.Item as="li"> <Button variant="danger" onClick={() => handleQuantityDecrement()}>
                <span> - </span>
              </Button>
                <span> {selectedQuantity} </span>
                <Button variant="primary" onClick={() => handleQuantityIncrement()}>
                  <span> + </span>
                </Button></ListGroup.Item>
            </ListGroup>

          </div>
          <Button variant="primary" onClick={handleAddToCart}>
            Add to Cart
          </Button>

        </Card.Body>
      </Card>
    </>
  );
};

export default Product;



