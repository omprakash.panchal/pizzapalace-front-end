import React, { createContext, useState, ReactNode } from 'react';
import { Pizza } from '../models/pizza';

interface CartContextProps {
    cartItems: Pizza[];
    addToCart: (pizza: Pizza, size: string, quantity: number, price: number) => void;
    removeFromCart: (pizzaId: number, size: string) => void;
    handleQuantityChange: (pizzaId: number, newQuantity: number, size: string) => void;
    clearCartItems: () => void;
}

export const CartContext = createContext<CartContextProps>({
    cartItems: [],
    addToCart: () => { },
    removeFromCart: () => { },
    handleQuantityChange: () => { },
    clearCartItems: () => { },
});

export const CartProvider: React.FC<{ children: ReactNode }> = ({ children }) => {
    const [cartItems, setCartItems] = useState<Pizza[]>([]);

    const addToCart = (pizza: Pizza, size: string, quantity: number, price: number) => {
        console.log("add to cart")
        setCartItems((prevCartItems: Pizza[]) => {
            const existingItem = prevCartItems.find((item) => item.pizzaId === pizza.pizzaId && item.size === size);

            if (existingItem) {
                // Item already exists, update the quantity for the existing size
                const updatedCartItems = prevCartItems.map((item) => {
                    if (item.pizzaId === pizza.pizzaId && item.size === size) {
                        return { ...item, quantity: item.quantity + quantity };
                    }
                    return item;
                });
                return updatedCartItems;
            } else {
                // Item does not exist, add a new item with the selected size
                const cartItem = { ...pizza, size, quantity, price };
                return [...prevCartItems, cartItem];
            }
        });
    };

    const removeFromCart = (pizzaId: number, size: string) => {
        // Remove the pizza from the cartItems state
        setCartItems((prevCartItems) =>
            prevCartItems.filter((item) => item.pizzaId !== pizzaId || item.size !== size)
        );
    };

    const handleQuantityChange = (pizzaId: number, newQuantity: number, size: string) => {
        // Update the quantity of the pizza in the cartItems state
        setCartItems((prevCartItems) =>
            prevCartItems.map((item) => {
                if (item.pizzaId === pizzaId && item.size === size) {
                    return { ...item, quantity: newQuantity };
                }
                return item;
            })
        );
    };

    const clearCartItems = () => {
        // Clear the cartItems state
        setCartItems([]);
    };

    return (
        <CartContext.Provider
            value={{
                cartItems,
                addToCart,
                removeFromCart,
                handleQuantityChange,
                clearCartItems,
            }}
        >
            {children}
        </CartContext.Provider>
    );
};
