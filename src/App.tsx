import './App.css';
import Content from './common/content';
import Footer from './common/footer';
import NavbarComponent from './common/navbar';
import { ToastContainer, Zoom, Flip, toast } from "react-toastify";
import { CartContext } from './contexts/CartContext';
function App() {
  return (
    <div className="App" style={{backgroundColor:"#EEE"}}>
        <NavbarComponent/>
        <ToastContainer position="bottom-center" theme="dark" draggable />
        <Content/>
        <Footer/>
    </div>
  );
}

export default App;
